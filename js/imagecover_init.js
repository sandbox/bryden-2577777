/**
 * Created by barndt on 26/08/15.
 */
jQuery(document).ready(function ($) {
    $('.imagecover').imagecover({
        runOnce : false,
        throttle: 200 ,
        css2    : false,
        preloadAllImages:false,
        loadingClass: 'ic-loading',
        addPreload: false
    });
});
