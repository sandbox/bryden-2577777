Introduction
------------
This module provides Imagecover functionality to Drupal. Imagecover is a
jQuery plugin that allows an image to cover all of its container. The
image will be automatically scaled and cropped with the focus on the centre
of the image, and it will entirely fill the container

 * For a full description of the module, visit the project page:
   [Imagecover](https://www.drupal.org/sandbox/bryden/2577777)

 * To submit bug reports and feature suggestions, or to track changes:
   [Issue tracking](https://www.drupal.org/project/issues/2577777)


REQUIREMENTS
------------
This module requires the following modules:
 * [The libraries module](https://www.drupal.org/ject/libraries)
 * [jQuery Update](https://www.drupal.org/project/jquery_update)
 * [The imagecover library](https://github.com/Metalchocobo/imagecover)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   [Installing drupal modules]
   (https://drupal.org/documentation/install/modules-themes/modules-7)
   for further information.
 * Install the following modules as you would normally:
   - [jquery_update](https://www.drupal.org/project/jquery_update)
   - [librarie](https://www.drupal.org/project/libraries)
 * Download the imagecover library and install it in your "libraries" folder
   (usually /sites/all/libraries)
 * Add the class "imagecover" to any element (a div, if you so choose) and
   the imagecover library will look for the first child element that is an img
   and it will scale and center crop that img to fit the div.


Configuration
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will target any element with "imagecover" class that has
an img child element. To remove this module, disable the module and clear
caches.


MAINTAINERS
-----------
Current maintainers:
 * Bryden Arndt (bryden) - [bryden](https://www.drupal.org/u/bryden)

Author of original Imagecover library:
 * Andrea Metalchocobo - [Andrea Metalchocobo](https://github.com/Metalchocobo)